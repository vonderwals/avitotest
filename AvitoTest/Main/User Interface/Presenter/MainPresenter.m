//
//  MainMainPresenter.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

#import "MainPresenter.h"
#import "MainWireframe.h"
#import "MainViewInterface.h"
#import "MainModuleDelegateInterface.h"

#import "SegmentModuleInterface.h"

@interface MainPresenter ()

@property (nonatomic, assign) NSInteger currentSegmentIndex;

@property (nonatomic, weak) id<MainViewInterface> userInterface;

@end

@implementation MainPresenter

- (void)setupUserInterfaceForPresentation:(id<MainViewInterface>)userInterface
{
    NSParameterAssert(userInterface != nil);
    _userInterface = userInterface;
}

- (void)setupInitialSegment:(NSInteger)segmentIndex
{
    _currentSegmentIndex = segmentIndex;
    [_userInterface updateSegmentIndex:_currentSegmentIndex];
    [self p_displaySegmentByIndex:_currentSegmentIndex];
}

#pragma mark - Private Methods

- (void)p_displaySegmentByIndex:(NSInteger)segmentIndex
{
    switch (segmentIndex)
    {
        case 0:
            [self.mainWireframe displayFirstSegment];
            break;
        case 1:
            [self.mainWireframe displaySecondSegment];
            break;
        default:
            break;
    }
}

#pragma mark - MainModuleInterface

- (void)segmentChangedAction:(NSInteger)segmentIndex
{
    _currentSegmentIndex = segmentIndex;
    [self p_displaySegmentByIndex:segmentIndex];
}

- (void)searchClickedAction:(NSString *)text
{
    switch (_currentSegmentIndex) {
        case 0:
            [self.firstSegmentModule searchKey:text];
            break;
        case 1:
            [self.secondSegmentModule searchKey:text];
            break;
        default:
            break;
    }
}

#pragma mark - MainInteractorOutput

@end
