//
//  ApplicationAssembly.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import <Typhoon/Typhoon.h>

#import "MainModuleAssembly.h"

@interface ApplicationAssembly : TyphoonAssembly

@property (nonatomic, weak) MainModuleAssembly *mainAssembly;

@end
