//
//  SegmentDisplayItem.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 10/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

@import UIKit;
@import Foundation;

@interface SegmentDisplayItem : NSObject

@property (nonatomic, strong, readonly) NSString *topText;

@property (nonatomic, strong, readonly) NSString *bottomText;

@property (nonatomic, copy, readonly) void (^imageBlock)(void (^)(UIImage *));

- (instancetype)initWithTopText:(NSString *)topText
                     bottomText:(NSString *)bottomText
                     imageBlock:(void (^)(void (^)(UIImage *))) imageBlock;

@end
