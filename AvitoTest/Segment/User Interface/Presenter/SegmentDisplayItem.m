//
//  SegmentDisplayItem.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 10/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import "SegmentDisplayItem.h"

@implementation SegmentDisplayItem

- (instancetype)initWithTopText:(NSString *)topText
                     bottomText:(NSString *)bottomText
                     imageBlock:(void (^)(void (^)(UIImage *)))imageBlock
{
    self = [super init];
    if (self)
    {
        _topText = topText;
        _bottomText = bottomText;
        _imageBlock = imageBlock;
    }
    return self;
}

@end
