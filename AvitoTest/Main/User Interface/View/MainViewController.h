//
//  MainMainViewController.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

@import UIKit;

#import "MainViewInterface.h"

@protocol MainModuleInterface;

NS_ASSUME_NONNULL_BEGIN

@interface MainViewController : UIViewController <MainViewInterface>

@property (weak, nonatomic, nullable) id<MainModuleInterface> eventHandler;

- (UIView *)containerView;

@end

NS_ASSUME_NONNULL_END
