//
//  SegmentSegmentPresenter.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

#import "SegmentModuleInterface.h"
#import "SegmentInteractorIO.h"

@class SegmentWireframe;

@protocol SegmentViewInterface;
@protocol SegmentModuleDelegateInterface;

NS_ASSUME_NONNULL_BEGIN

@interface SegmentPresenter : NSObject <SegmentModuleInterface, SegmentInteractorOutput>

typedef NS_ENUM(NSInteger, SegmentMode)
{
    ItunesMode,
    GitHubMode
};

@property (strong, nonatomic, nullable) id<SegmentInteractorInput> segmentInteractor;

@property (weak, nonatomic, nullable) SegmentWireframe *segmentWireframe;

@property (weak, nonatomic, nullable) id<SegmentModuleDelegateInterface> segmentModuleDelegate;

- (void)configureUserInterfaceForPresentation:(id<SegmentViewInterface>)userInterface mode:(SegmentMode) mode;

@property (nonatomic, assign) SegmentMode segmentMode;

@end

NS_ASSUME_NONNULL_END
