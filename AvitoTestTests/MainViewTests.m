//
//  MainViewTests.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 11/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "MainViewController.h"
#import "MainModuleInterface.h"

@interface MainViewTests : XCTestCase

@property (nonatomic, strong) MainViewController *mainView;

@end

@implementation MainViewTests

- (void)setUp
{
    [super setUp];
    
    self.mainView = [[MainViewController alloc] init];
}

- (void)tearDown
{

    [super tearDown];
}


@end
