//
//  MainMainInteractor.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

#import "MainInteractorIO.h"

@protocol MainDataManagerInterface;

NS_ASSUME_NONNULL_BEGIN

@interface MainInteractor : NSObject <MainInteractorInput>

@property (weak, nonatomic, nullable) id<MainInteractorOutput> output;

@property (strong, nonatomic, nullable) id<MainDataManagerInterface> dataManager;

@end

NS_ASSUME_NONNULL_END
