//
//  SegmentSegmentModuleAssembly.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

#import "SegmentModuleAssembly.h"

#import "SegmentWireframe.h"
#import "SegmentPresenter.h"
#import "SegmentInteractor.h"

@implementation SegmentModuleAssembly

- (SegmentWireframe *)segmentWireframeWithDataManager:(id<SegmentDataManagerInterface>)dataManager
{
    return [TyphoonDefinition withClass:[SegmentWireframe class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(segmentPresenter) with:[self segmentPresenterWithDataManager:dataManager]];
    }];
}

- (SegmentPresenter *)segmentPresenterWithDataManager:(id<SegmentDataManagerInterface>)dataManager
{
    return [TyphoonDefinition withClass:[SegmentPresenter class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(segmentWireframe) with:[self segmentWireframeWithDataManager:dataManager]];
        [definition injectProperty:@selector(segmentInteractor) with:[self segmentInteractorWithDataManager:dataManager]];
    }];
}

- (SegmentInteractor *)segmentInteractorWithDataManager:(id<SegmentDataManagerInterface>)dataManager
{
    return [TyphoonDefinition withClass:[SegmentInteractor class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(output) with:[self segmentPresenterWithDataManager:dataManager]];
        [definition injectProperty:@selector(dataManager) with:dataManager];
    }];
}

@end