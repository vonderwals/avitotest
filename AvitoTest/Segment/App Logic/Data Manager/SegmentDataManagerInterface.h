//
//  SegmentSegmentDataManagerInterface.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

@import Foundation;

NS_ASSUME_NONNULL_BEGIN

@protocol SegmentDataManagerInterface <NSObject>
@required

- (void)findDataByKey:(NSString *)key withCompletionHandler:(void (^) (NSArray *, NSError *)) handler;

@end

NS_ASSUME_NONNULL_END
