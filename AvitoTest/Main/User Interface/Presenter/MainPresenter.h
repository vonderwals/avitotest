//
//  MainMainPresenter.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

#import "MainModuleInterface.h"
#import "MainInteractorIO.h"

@class MainWireframe;

@protocol MainViewInterface;
@protocol MainModuleDelegateInterface;

@protocol SegmentModuleInterface;

NS_ASSUME_NONNULL_BEGIN

@interface MainPresenter : NSObject <MainModuleInterface, MainInteractorOutput>

@property (strong, nonatomic, nullable) id<MainInteractorInput> mainInteractor;

@property (weak, nonatomic, nullable) MainWireframe *mainWireframe;

@property (weak, nonatomic, nullable) id<MainModuleDelegateInterface> mainModuleDelegate;

@property (weak, nonatomic, nullable) id<SegmentModuleInterface> firstSegmentModule;
@property (weak, nonatomic, nullable) id<SegmentModuleInterface> secondSegmentModule;

- (void)setupUserInterfaceForPresentation:(id<MainViewInterface>)userInterface;
- (void)setupInitialSegment:(NSInteger)segmentIndex;

@end

NS_ASSUME_NONNULL_END
