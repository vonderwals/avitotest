//
//  LeftAvatarCell.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 10/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import "LeftAvatarCell.h"
#import "SegmentDisplayItem.h"

#import <Masonry/Masonry.h>

@interface LeftAvatarCell ()

@property (nonatomic, strong) UIImageView *avatarImageView;

@property (nonatomic, strong) UIView *textContainer;

@property (nonatomic, strong) UILabel *topLabel;

@property (nonatomic, strong) UILabel *bottomLabel;

@end

@implementation LeftAvatarCell

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        [self p_commonInit];
    }
    return self;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self)
    {
        [self p_commonInit];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self p_commonInit];
    }
    return self;
}

- (void)prepareForReuse
{
    _avatarImageView.image = nil;
    _avatarImageView.alpha = 0;
}

- (void)p_commonInit
{
    _avatarImageView = [[UIImageView alloc] init];
    _avatarImageView.alpha = 0;
    _avatarImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(p_showIcon)];
    [_avatarImageView addGestureRecognizer:tapRecognizer];
    [self addSubview:_avatarImageView];
    [_avatarImageView mas_makeConstraints:^(MASConstraintMaker *make)
    {
        make.width.equalTo(@60);
        make.height.equalTo(@60);
        make.centerY.equalTo(self);
        make.left.equalTo(self).with.offset(5);
    }];
    
    _textContainer = [[UIView alloc] init];
    [self addSubview:_textContainer];
    [_textContainer mas_makeConstraints:^(MASConstraintMaker *make)
    {
        make.left.equalTo(_avatarImageView.mas_right);
        make.right.equalTo(self);
        make.top.equalTo(self);
        make.bottom.equalTo(self);
    }];
    
    _topLabel = [[UILabel alloc] init];
    [_textContainer addSubview:_topLabel];
    [_topLabel mas_makeConstraints:^(MASConstraintMaker *make)
    {
        make.left.equalTo(_textContainer).with.offset(5);
        make.top.equalTo(_textContainer).with.offset(5);
    }];
    
    _bottomLabel = [[UILabel alloc] init];
    [_textContainer addSubview:_bottomLabel];
    [_bottomLabel mas_makeConstraints:^(MASConstraintMaker *make)
    {
        make.left.equalTo(_textContainer).with.offset(5);
        make.bottom.equalTo(_textContainer).with.offset(-5);
    }];
}

- (void)setupWithDisplayItem:(SegmentDisplayItem *)item
{
    _topLabel.text = item.topText;
    _bottomLabel.text = item.bottomText;
    
    item.imageBlock(^(UIImage *image)
    {
        _avatarImageView.image = image;
        [UIView animateWithDuration:0.5 animations:^
        {
            _avatarImageView.alpha = 1;
        }];
    });
}

- (void)p_showIcon
{
    self.presentBlock(_avatarImageView.image, _avatarImageView.frame);
}

@end
