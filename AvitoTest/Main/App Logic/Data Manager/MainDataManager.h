//
//  MainMainDataManager.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

#import "MainDataManagerInterface.h"

NS_ASSUME_NONNULL_BEGIN

@interface MainDataManager : NSObject <MainDataManagerInterface>

@end

NS_ASSUME_NONNULL_END
