//
//  MainMainModuleAssembly.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

#import "MainModuleAssembly.h"

#import "MainWireframe.h"
#import "MainPresenter.h"
#import "MainInteractor.h"
#import "MainViewController.h"

#import "iTunesDataManager.h"
#import "GitHubDataManager.h"

#import "SegmentModuleAssembly.h"

@implementation MainModuleAssembly

- (MainWireframe *)mainWireframe
{
    return [TyphoonDefinition withClass:[MainWireframe class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(mainPresenter) with:[self mainPresenter]];
        [definition injectProperty:@selector(firstSegmentWireframe)
                              with:[self.segmentAssembly segmentWireframeWithDataManager:[self iTunesDataManager]]];
        [definition injectProperty:@selector(secondSegmentWireframe)
                              with:[self.segmentAssembly segmentWireframeWithDataManager:[self gitHubDataManager]]];
    }];
}

- (MainPresenter *)mainPresenter
{
    return [TyphoonDefinition withClass:[MainPresenter class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(mainWireframe) with:[self mainWireframe]];
        [definition injectProperty:@selector(mainInteractor) with:[self mainInteractor]];
        [definition injectProperty:@selector(firstSegmentModule) with:[self.segmentAssembly segmentPresenterWithDataManager:[self iTunesDataManager]]];
        [definition injectProperty:@selector(secondSegmentModule) with:[self.segmentAssembly segmentPresenterWithDataManager:[self gitHubDataManager]]];
    }];
}

- (MainInteractor *)mainInteractor
{
    return [TyphoonDefinition withClass:[MainInteractor class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(output) with:[self mainPresenter]];
    }];
}


- (MainViewController *)mainVC
{
    return [TyphoonDefinition withClass:[MainViewController class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(eventHandler) with:[self mainPresenter]];
    }];
}

- (id<SegmentDataManagerInterface>)iTunesDataManager
{
    return [TyphoonDefinition withClass:[iTunesDataManager class]];
}

- (id<SegmentDataManagerInterface>)gitHubDataManager
{
    return [TyphoonDefinition withClass:[GitHubDataManager class]];
}

@end