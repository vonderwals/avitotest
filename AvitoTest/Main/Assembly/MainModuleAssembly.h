//
//  MainMainModuleAssembly.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

#import <Typhoon/Typhoon.h>

@class MainWireframe;
@class MainViewController;

@class SegmentModuleAssembly;

@interface MainModuleAssembly : TyphoonAssembly

@property (nonatomic, weak) SegmentModuleAssembly *segmentAssembly;

- (MainWireframe *) mainWireframe;

- (MainViewController *)mainVC;

@end
