//
//  MainMainWireframe.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

#import "MainWireframe.h"
#import "MainPresenter.h"
#import "MainViewController.h"

#import "SegmentWireframe.h"

#import "SegmentPresenter.h"

@interface MainWireframe ()

@property (nonatomic, strong) MainViewController *mainVC;

@end

@implementation MainWireframe

- (void)configureVC:(MainViewController *)mainVC
{
    _mainVC = mainVC;
    [self.mainPresenter setupUserInterfaceForPresentation:mainVC];
    [self.mainPresenter setupInitialSegment:0];
}

- (void)displayFirstSegment
{
    [self.secondSegmentWireframe dismissSegment];
    [self.firstSegmentWireframe presentiTunesSegmentInView:[_mainVC containerView]];
}

- (void)displaySecondSegment
{
    [self.firstSegmentWireframe dismissSegment];
    [self.secondSegmentWireframe presentGitHubSegmentInView:[_mainVC containerView]];
}

@end
