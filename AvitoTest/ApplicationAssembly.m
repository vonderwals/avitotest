//
//  ApplicationAssembly.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import "ApplicationAssembly.h"

#import "AppDelegate.h"
#import "MainViewController.h"

@implementation ApplicationAssembly

- (AppDelegate *)appDelegate
{
    return [TyphoonDefinition withClass:[AppDelegate class] configuration:^(TyphoonDefinition *definition)
    {
        [definition injectProperty:@selector(window) with:[self mainWindow]];
        [definition injectProperty:@selector(mainWireframe) with:[self.mainAssembly mainWireframe]];
    }];
}

- (UIWindow *)mainWindow
{
    return [TyphoonDefinition withClass:[UIWindow class] configuration:^(TyphoonDefinition *definition)
    {
        [definition useInitializer:@selector(initWithFrame:) parameters:^(TyphoonMethod *initializer)
        {
             [initializer injectParameterWith:[NSValue valueWithCGRect:[[UIScreen mainScreen] bounds]]];
        }];
        [definition injectProperty:@selector(rootViewController) with:[self.mainAssembly mainVC]];
    }];
}

@end
