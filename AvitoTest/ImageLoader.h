//
//  ImageLoader.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 10/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

@import UIKit;
@import Foundation;

@interface ImageLoader : NSObject

+ (instancetype)sharedImageLoader;

- (UIImage *)loadImageByURL:(NSURL *)url completionHandler:(void(^)(UIImage *))handler;

@end
