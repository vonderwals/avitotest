//
//  MainPresenterTests.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 11/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import <XCTest/XCTest.h>

#import <OCMock/OCMock.h>

#import "MainPresenter.h"
#import "MainViewInterface.h"
#import "MainWireframe.h"
#import "SegmentModuleInterface.h"

@interface MainPresenterTests : XCTestCase

@property (nonatomic, strong) MainPresenter *mainPresenter;
@property (nonatomic, strong) id userInterface;
@property (nonatomic, strong) id wireframe;

@property (nonatomic, strong) id firstSegmentModule;
@property (nonatomic, strong) id secondSegmentModule;

@end

@implementation MainPresenterTests

- (void)setUp
{
    [super setUp];
    
    self.mainPresenter = [[MainPresenter alloc] init];
    self.userInterface = OCMProtocolMock(@protocol(MainViewInterface));
    self.wireframe = OCMClassMock([MainWireframe class]);
    self.firstSegmentModule = OCMProtocolMock(@protocol(SegmentModuleInterface));
    self.secondSegmentModule = OCMProtocolMock(@protocol(SegmentModuleInterface));
    
    self.mainPresenter.mainWireframe = self.wireframe;
    [self.mainPresenter setupUserInterfaceForPresentation:self.userInterface];
    self.mainPresenter.firstSegmentModule = self.firstSegmentModule;
    self.mainPresenter.secondSegmentModule = self.secondSegmentModule;
}

- (void)testInitialFirstSegment
{
    NSInteger segmentIndex = 0;
    
    OCMExpect([self.wireframe displayFirstSegment]);
    OCMExpect([self.userInterface updateSegmentIndex:segmentIndex]);
    
    [self.mainPresenter setupInitialSegment:segmentIndex];
}

- (void)testInitialSecondSegment
{
    NSInteger segmentIndex = 1;
    
    OCMExpect([self.wireframe displaySecondSegment]);
    OCMExpect([self.userInterface updateSegmentIndex:segmentIndex]);
    
    [self.mainPresenter setupInitialSegment:segmentIndex];
}

- (void)testToggleToFirstSegment
{
    OCMExpect([self.wireframe displayFirstSegment]);
    
    [self.mainPresenter segmentChangedAction:0];
}

- (void)testToggleToSecondSegment
{
    OCMExpect([self.wireframe displaySecondSegment]);
    
    [self.mainPresenter segmentChangedAction:1];
}

- (void)testSearchAfterInitialFirstSegment
{
    NSString *key = @"test";
    NSInteger segmentIndex = 0;
    OCMExpect([self.firstSegmentModule searchKey:key]);
    OCMExpect([self.wireframe displayFirstSegment]);
    OCMExpect([self.userInterface updateSegmentIndex:segmentIndex]);
    
    [self.mainPresenter setupInitialSegment:segmentIndex];
    [self.mainPresenter searchClickedAction:key];
}

- (void)testSearchAfterInitialSecondSegment
{
    NSString *key = @"test";
    NSInteger segmentIndex = 1;
    OCMExpect([self.secondSegmentModule searchKey:key]);
    OCMExpect([self.wireframe displaySecondSegment]);
    OCMExpect([self.userInterface updateSegmentIndex:segmentIndex]);
    
    [self.mainPresenter setupInitialSegment:segmentIndex];
    [self.mainPresenter searchClickedAction:key];
}

- (void)testSearchAfterToggleToFirstSegment
{
    NSString *key = @"test";
    OCMExpect([self.firstSegmentModule searchKey:key]);
    OCMExpect([self.wireframe displayFirstSegment]);
    
    [self.mainPresenter segmentChangedAction:0];
    [self.mainPresenter searchClickedAction:key];
}

- (void)testSearchAfterToggleToSecondSegment
{
    NSString *key = @"test";
    OCMExpect([self.secondSegmentModule searchKey:key]);
    OCMExpect([self.wireframe displaySecondSegment]);
    
    [self.mainPresenter segmentChangedAction:1];
    [self.mainPresenter searchClickedAction:key];
}


- (void)tearDown
{
    OCMVerifyAll(self.firstSegmentModule);
    OCMVerifyAll(self.secondSegmentModule);
    OCMVerifyAll(self.wireframe);
    OCMVerifyAll(self.userInterface);
    
    [super tearDown];
}

@end
