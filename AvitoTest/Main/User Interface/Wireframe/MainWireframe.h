//
//  MainMainWireframe.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

@import Foundation;
@import UIKit;

@class MainPresenter;

@class SegmentWireframe;
@class MainViewController;

NS_ASSUME_NONNULL_BEGIN

@interface MainWireframe : NSObject

@property (strong, nonatomic, nullable) MainPresenter *mainPresenter;

@property (strong, nonatomic, nullable) SegmentWireframe *firstSegmentWireframe;

@property (strong, nonatomic, nullable) SegmentWireframe *secondSegmentWireframe;

- (void)configureVC:(MainViewController *)mainVC;

- (void)displayFirstSegment;
- (void)displaySecondSegment;

@end

NS_ASSUME_NONNULL_END
