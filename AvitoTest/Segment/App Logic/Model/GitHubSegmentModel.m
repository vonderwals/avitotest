//
//  GitHubSegmentModel.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 10/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import "GitHubSegmentModel.h"

@implementation GitHubSegmentModel

- (instancetype)initWithAvatarURL:(NSURL *)url login:(NSString *)login account:(NSString *)account
{
    self = [super init];
    if (self)
    {
        _avatarURL = url;
        _login = login;
        _account = account;
    }
    return self;
}

@end
