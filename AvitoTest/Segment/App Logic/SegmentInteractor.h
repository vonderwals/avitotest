//
//  SegmentSegmentInteractor.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

#import "SegmentInteractorIO.h"

@protocol SegmentDataManagerInterface;

NS_ASSUME_NONNULL_BEGIN

@interface SegmentInteractor : NSObject <SegmentInteractorInput>

@property (weak, nonatomic, nullable) id<SegmentInteractorOutput> output;

@property (strong, nonatomic, nullable) id<SegmentDataManagerInterface> dataManager;

@end

NS_ASSUME_NONNULL_END
