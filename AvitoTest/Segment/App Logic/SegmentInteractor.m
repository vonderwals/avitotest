//
//  SegmentSegmentInteractor.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

#import "SegmentInteractor.h"
#import "SegmentDataManagerInterface.h"

@implementation SegmentInteractor

#pragma mark - SegmentInteractorInput

-(void)findDataByKey:(NSString *)key
{
    NSParameterAssert(key != nil);
    
    [self.dataManager findDataByKey:key withCompletionHandler:^(NSArray *models, NSError *error)
    {
        if(error)
        {
            [self.output errorOccurred:error];
        }
        else if(models)
        {
            [self.output modelsLoaded:models];
        }
    }];
}

@end
