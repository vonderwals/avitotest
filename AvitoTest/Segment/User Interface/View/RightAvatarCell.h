//
//  RightAvatarCell.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 10/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SegmentDisplayItem;

@interface RightAvatarCell : UITableViewCell

- (void)setupWithDisplayItem:(SegmentDisplayItem *)item;

@property (nonatomic, copy) void (^presentBlock)(UIImage *image, CGRect originalFrame);


@end
