//
//  GitHubDataManager.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SegmentDataManagerInterface.h"

@interface GitHubDataManager : NSObject <SegmentDataManagerInterface>

@end
