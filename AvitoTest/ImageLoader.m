//
//  ImageLoader.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 10/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import "ImageLoader.h"

@implementation ImageLoader
{
    NSCache *_imageCache;
}

- (instancetype)init
{
    self = [super init];
    if(self)
    {
        _imageCache = [NSCache new];
    }
    return self;
}

+ (instancetype)sharedImageLoader
{
    static ImageLoader *imageLoader;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        imageLoader = [[ImageLoader alloc] init];
    });
    
    return imageLoader;
}

-(UIImage *)loadImageByURL:(NSURL *)url completionHandler:(void(^)(UIImage *))handler
{
    UIImage *image = nil;
    NSData *data = [_imageCache objectForKey:[url path]];
    if(data)
    {
        image = [UIImage imageWithData:data];
    }
    
    if(!image)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
        {
           NSData *imageData = [NSData dataWithContentsOfURL:url];
           UIImage *image = [UIImage imageWithData:imageData];
           if(image)
           {
               [_imageCache setObject:imageData forKey:[url path]];
               handler(image);
           }
        });
    }
    
    return image;
}

@end
