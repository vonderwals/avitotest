//
//  SegmentSegmentPresenter.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

#import "SegmentPresenter.h"
#import "SegmentWireframe.h"
#import "SegmentViewInterface.h"
#import "SegmentModuleDelegateInterface.h"

#import "SegmentModelConverter.h"

@interface SegmentPresenter ()

@property (nonatomic, weak) id<SegmentViewInterface> userInterface;

@property (nonatomic, strong) SegmentModelConverter *converter;

@end

@implementation SegmentPresenter

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _converter = [SegmentModelConverter new];
    }
    return self;
}

- (void)configureUserInterfaceForPresentation:(id<SegmentViewInterface>)userInterface mode:(SegmentMode)mode
{
    NSParameterAssert(userInterface != nil);
    _userInterface = userInterface;
    
    _segmentMode = mode;
    if(_segmentMode == ItunesMode)
    {
        [self.userInterface setupViewWithInverted:NO];
    }
    else if(_segmentMode == GitHubMode)
    {
        [self.userInterface setupViewWithInverted:YES];
    }
}

#pragma mark - SegmentModuleInterface

- (void)searchKey:(NSString *)key
{
    [self.segmentInteractor findDataByKey:key];
}

#pragma mark - SegmentInteractorOutput

- (void)errorOccurred:(NSError *)error
{
    //TODO Handle Error Detail
    [self.segmentWireframe presentErrorAlertWithTitle:@"Error" description:@"Error Occured!"];
}

- (void)modelsLoaded:(NSArray *)models
{
    NSArray *items = [_converter displayItemsFromModels:models];
    if(items.count == 0)
    {
        [self.segmentWireframe presentErrorAlertWithTitle:@"No data" description:@"There isn't any data for this search key"];
    }
    [_userInterface diplayItems:items];
}

@end
