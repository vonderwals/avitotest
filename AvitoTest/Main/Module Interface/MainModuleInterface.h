//
//  MainMainModuleInterface.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

@import Foundation;

NS_ASSUME_NONNULL_BEGIN

/**
*  Main module
*/
@protocol MainModuleInterface <NSObject>
@required

- (void)segmentChangedAction:(NSInteger)segmentIndex;

- (void)searchClickedAction:(NSString *)text;

@end

NS_ASSUME_NONNULL_END
