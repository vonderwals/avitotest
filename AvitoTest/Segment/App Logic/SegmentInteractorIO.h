//
//  SegmentSegmentInteractorIO.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

@import Foundation;

NS_ASSUME_NONNULL_BEGIN

@protocol SegmentInteractorInput <NSObject>
@required

- (void)findDataByKey:(NSString *)key;

@end

@protocol SegmentInteractorOutput <NSObject>
@required

- (void)modelsLoaded:(NSArray *)models;

- (void)errorOccurred:(NSError *)error;

@end

NS_ASSUME_NONNULL_END
