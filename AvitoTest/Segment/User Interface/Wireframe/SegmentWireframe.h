//
//  SegmentSegmentWireframe.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

@import Foundation;
@import UIKit;

@class SegmentPresenter;

NS_ASSUME_NONNULL_BEGIN

@interface SegmentWireframe : NSObject

@property (strong, nonatomic, nullable) SegmentPresenter *segmentPresenter;

- (void)presentiTunesSegmentInView:(UIView *)view;

- (void)presentGitHubSegmentInView:(UIView *)view;

- (void)dismissSegment;

- (void)presentErrorAlertWithTitle:(NSString *)title description:(NSString *)description;

@end

NS_ASSUME_NONNULL_END
