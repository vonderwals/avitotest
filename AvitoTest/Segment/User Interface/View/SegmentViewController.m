//
//  SegmentSegmentViewController.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

#import "SegmentViewController.h"
#import "SegmentModuleInterface.h"

#import "LeftAvatarCell.h"
#import "RightAvatarCell.h"

#import "SegmentDisplayItem.h"

#import <Masonry/Masonry.h>

@interface SegmentViewController () <UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSArray *items;

@property (nonatomic, assign) BOOL inverted;

//TODO Refactor
@property (nonatomic, strong) UIImageView *zoomedImageView;
@property (nonatomic, assign) CGRect originalFrame;

@end

@implementation SegmentViewController

static NSString * const kLeftAvatarCell = @"LeftAvatarCell";
static NSString * const kRightAvatarCell = @"RightAvatarCell";

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _items = [NSMutableArray new];
        
        _tableView = [[UITableView alloc] init];
        _tableView.dataSource = self;
        _tableView.rowHeight = 60;
        _tableView.allowsSelection = NO;
        [_tableView registerClass:[LeftAvatarCell class] forCellReuseIdentifier:kLeftAvatarCell];
        [_tableView registerClass:[RightAvatarCell class] forCellReuseIdentifier:kRightAvatarCell];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.view addSubview:_tableView];
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make)
    {
        make.edges.equalTo(self.view);
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if(!_inverted)
    {
        cell = [self p_prepareItunesCellWithIndexPath:indexPath];
    }
    else
    {
        cell = [self p_prepareItunesCellWithIndexPath:indexPath];
    }

    return cell;
}

#pragma mark - Private Methods

- (void)p_zoomImageViewWithImage:(UIImage *)image originalFrame:(CGRect)originalFrame cell:(UITableViewCell *)cell
{
    if(_zoomedImageView)
        return;
    
    //Hack(
    UIView *mainView = self.view.superview.superview;
    CGRect newFrame = mainView.frame;
    
    _zoomedImageView = [[UIImageView alloc] initWithImage:image];
    _zoomedImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(p_unzoomImageView)];
    [_zoomedImageView addGestureRecognizer:tapRecognizer];
    _originalFrame = [cell convertRect:originalFrame toView:mainView];
    _zoomedImageView.frame = _originalFrame;
    [mainView addSubview:_zoomedImageView];
    
    [UIView animateWithDuration:0.5 animations:^
     {
         _zoomedImageView.frame = newFrame;
     }];
}

- (void)p_unzoomImageView
{
    if(!_zoomedImageView)
        return;
    
    [UIView animateWithDuration:0.5 animations:^
    {
        _zoomedImageView.frame = _originalFrame;
    }
    completion:^(BOOL finished)
    {
        [_zoomedImageView removeFromSuperview];
        _zoomedImageView = nil;
    }];
}

//TODO Refactor
- (UITableViewCell *)p_prepareItunesCellWithIndexPath:(NSIndexPath *)indexPath
{
    SegmentDisplayItem *item = _items[indexPath.row];
    UITableViewCell *cell;
    if((indexPath.row + 1) % 2 == 0)
    {
        LeftAvatarCell *leftCell = [self.tableView dequeueReusableCellWithIdentifier:kLeftAvatarCell];
        [leftCell setupWithDisplayItem:item];
        
        __weak typeof(leftCell) weakLeftCell = leftCell;
        __weak typeof(self) weakSelf = self;
        leftCell.presentBlock = ^(UIImage *image, CGRect oFrame)
        {
            __strong typeof(weakLeftCell) strongLeftCell = weakLeftCell;
            __strong typeof(weakSelf) strongSelf = weakSelf;
            
            [strongSelf p_zoomImageViewWithImage:image originalFrame:oFrame cell:strongLeftCell];
        };
        cell = leftCell;
    }
    else
    {
        RightAvatarCell *rightCell = [self.tableView dequeueReusableCellWithIdentifier:kRightAvatarCell];
        [rightCell setupWithDisplayItem:item];
        
        __weak typeof(rightCell) weakRightCell = rightCell;
        __weak typeof(self) weakSelf = self;
        rightCell.presentBlock = ^(UIImage *image, CGRect oFrame)
        {
            __strong typeof(weakRightCell) strongRightCell = weakRightCell;
            __strong typeof(weakSelf) strongSelf = weakSelf;
            
            [strongSelf p_zoomImageViewWithImage:image originalFrame:oFrame cell:strongRightCell];
        };
        cell = rightCell;
    }
    return cell;
}

//TODO Refactor
- (UITableViewCell *)p_prepareGitHubCellWithIndexPath:(NSIndexPath *)indexPath
{
    SegmentDisplayItem *item = _items[indexPath.row];
    UITableViewCell *cell;
    if((indexPath.row + 1) % 2 == 0)
    {
        RightAvatarCell *rightCell = [self.tableView dequeueReusableCellWithIdentifier:kRightAvatarCell];
        [rightCell setupWithDisplayItem:item];
        __weak typeof(rightCell) weakRightCell = rightCell;
        __weak typeof(self) weakSelf = self;
        rightCell.presentBlock = ^(UIImage *image, CGRect oFrame)
        {
            __strong typeof(weakRightCell) strongRightCell = weakRightCell;
            __strong typeof(weakSelf) strongSelf = weakSelf;
            
            [strongSelf p_zoomImageViewWithImage:image originalFrame:oFrame cell:strongRightCell];
        };
        cell = rightCell;
    }
    else
    {
        LeftAvatarCell *leftCell = [self.tableView dequeueReusableCellWithIdentifier:kLeftAvatarCell];
        [leftCell setupWithDisplayItem:item];
        __weak typeof(leftCell) weakLeftCell = leftCell;
        __weak typeof(self) weakSelf = self;
        leftCell.presentBlock = ^(UIImage *image, CGRect oFrame)
        {
            __strong typeof(weakLeftCell) strongLeftCell = weakLeftCell;
            __strong typeof(weakSelf) strongSelf = weakSelf;
            
            [strongSelf p_zoomImageViewWithImage:image originalFrame:oFrame cell:strongLeftCell];
        };
        cell = leftCell;
    }
    return cell;
}


#pragma mark - SegmentViewInterface

- (void)diplayItems:(NSArray *)items
{
    _items = items;
    [self.tableView reloadData];
}

- (void)setupViewWithInverted:(BOOL)inverted
{
    _inverted = inverted;
}

@end
