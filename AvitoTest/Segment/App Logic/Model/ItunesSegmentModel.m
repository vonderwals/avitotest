//
//  ItunesSegmentModel.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 10/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import "ItunesSegmentModel.h"

@implementation ItunesSegmentModel

- (instancetype)initWithAvatarURL:(NSURL *)url title:(NSString *)title author:(NSString *)author
{
    self = [super init];
    if(self)
    {
        _avatarURL = url;
        _title = title;
        _author = author;
    }
    return self;
}

@end
