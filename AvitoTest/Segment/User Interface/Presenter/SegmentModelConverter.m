//
//  SegmenModelConverter.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 10/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import "SegmentModelConverter.h"

#import "SegmentDisplayItem.h"
#import "ItunesSegmentModel.h"
#import "GitHubSegmentModel.h"
#import "ImageLoader.h"

@implementation SegmentModelConverter

- (NSArray *)displayItemsFromModels:(NSArray *)models
{
    NSParameterAssert(models != nil);
    
    NSMutableArray *items = [NSMutableArray new];
    
    for(id model in models)
    {
        SegmentDisplayItem *item = [self p_displayItemFromModel:model];
        [items addObject:item];
    }
    
    return [items copy];
}

- (SegmentDisplayItem *)p_displayItemFromModel:(id)model
{
    SegmentDisplayItem *displayItem;
    if([model isKindOfClass:[ItunesSegmentModel class]])
    {
        ItunesSegmentModel *itunesModel = (ItunesSegmentModel *)model;
        displayItem = [[SegmentDisplayItem alloc] initWithTopText:itunesModel.title
                                                       bottomText:itunesModel.author
                                                       imageBlock:[self p_imageBlockFromURL:itunesModel.avatarURL]];
    }
    else if ([model isKindOfClass:[GitHubSegmentModel class]])
    {
        GitHubSegmentModel *gitHubModel = (GitHubSegmentModel *)model;
        displayItem = [[SegmentDisplayItem alloc] initWithTopText:gitHubModel.login
                                                       bottomText:gitHubModel.account
                                                       imageBlock:[self p_imageBlockFromURL:gitHubModel.avatarURL]];
    }
    else
    {
        //TODO Implement with NSError
        [NSException raise:@"Unknown model class" format:@"Unknown model class: %@", [model class]];
    }
    
    return displayItem;
}

- (void (^)(void (^)(UIImage *))) p_imageBlockFromURL:(NSURL *)url;
{
    return ^(void (^callback)(UIImage *))
    {
        UIImage *image = [[ImageLoader sharedImageLoader] loadImageByURL:url completionHandler:^(UIImage *image)
        {
            dispatch_async(dispatch_get_main_queue(), ^
            {
                callback(image);
            });
        }];
        
        if(image)
        {
            callback(image);
        }
    };
}

@end
