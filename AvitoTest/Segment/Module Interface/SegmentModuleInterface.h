//
//  SegmentSegmentModuleInterface.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

@import Foundation;

NS_ASSUME_NONNULL_BEGIN

/**
*  Segment module
*/
@protocol SegmentModuleInterface <NSObject>
@required

- (void)searchKey:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
