//
//  AppDelegate.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import "AppDelegate.h"

#import "MainWireframe.h"

@interface AppDelegate ()

@property (nonatomic, strong) MainWireframe *mainWireframe;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self.mainWireframe configureVC:(MainViewController *)self.window.rootViewController];
    
    [self.window makeKeyAndVisible];
    return YES;
}

@end
