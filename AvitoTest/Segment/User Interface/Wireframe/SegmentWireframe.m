//
//  SegmentSegmentWireframe.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

#import "SegmentWireframe.h"
#import "SegmentPresenter.h"
#import "SegmentViewController.h"

#import <Masonry/Masonry.h>

@interface SegmentWireframe ()

@property (nonatomic, strong) SegmentViewController *segmentVC;

@end

@implementation SegmentWireframe

- (instancetype)init
{
    self = [super init];
    if (self)
    {

    }
    return self;
}

- (void)presentiTunesSegmentInView:(UIView *)view
{
    if(!_segmentVC)
    {
        _segmentVC = [SegmentViewController new];
        _segmentVC.eventHandler = self.segmentPresenter;
        [self.segmentPresenter configureUserInterfaceForPresentation:_segmentVC mode:ItunesMode];
    }

    [view addSubview:_segmentVC.view];
    [_segmentVC.view mas_makeConstraints:^(MASConstraintMaker *make)
    {
        make.edges.equalTo(view).insets(UIEdgeInsetsZero);
    }];
}

- (void)presentGitHubSegmentInView:(UIView *)view
{
    if(!_segmentVC)
    {
        _segmentVC = [SegmentViewController new];
        _segmentVC.eventHandler = self.segmentPresenter;
        [self.segmentPresenter configureUserInterfaceForPresentation:_segmentVC mode:GitHubMode];
    }
    
    [view addSubview:_segmentVC.view];
    [_segmentVC.view mas_makeConstraints:^(MASConstraintMaker *make)
    {
         make.edges.equalTo(view).insets(UIEdgeInsetsZero);
    }];
}

- (void)dismissSegment
{
    [_segmentVC.view removeFromSuperview];
    [_segmentVC removeFromParentViewController];
}

- (void)presentErrorAlertWithTitle:(NSString *)title description:(NSString *)description
{
    UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:title
                                                       message:description
                                                      delegate:self
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles: nil];
    [alerView show];
}

@end
