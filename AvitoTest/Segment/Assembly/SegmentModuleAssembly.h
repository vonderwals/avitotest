//
//  SegmentSegmentModuleAssembly.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

#import <Typhoon/Typhoon.h>

@class SegmentWireframe;
@class SegmentPresenter;
@protocol SegmentDataManagerInterface;

@interface SegmentModuleAssembly : TyphoonAssembly

- (SegmentWireframe *) segmentWireframeWithDataManager:(id<SegmentDataManagerInterface>) dataManager;

- (SegmentPresenter *)segmentPresenterWithDataManager:(id<SegmentDataManagerInterface>) dataManager;

@end
