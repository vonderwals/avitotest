//
//  GitHubDataManager.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import "GitHubDataManager.h"

#import "GitHubSegmentModel.h"

@implementation GitHubDataManager

static NSString * const kGitHubURL = @"https://api.github.com/search/users";

-(void)findDataByKey:(NSString *)key withCompletionHandler:(void (^) (NSArray *, NSError *)) handler
{
    NSParameterAssert(key != nil);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
    {
       NSURLComponents *components = [NSURLComponents componentsWithString:kGitHubURL];
       NSURLQueryItem *q = [NSURLQueryItem queryItemWithName:@"q" value:key];
       components.queryItems = @[q];
       
       [[[NSURLSession sharedSession] dataTaskWithURL:components.URL completionHandler:^(NSData * __nullable data, NSURLResponse * __nullable response, NSError * __nullable error)
        {
             if(data)
             {
                 NSError *error = nil;
                 NSArray *models = [self p_parseData:data withError:&error];
                 dispatch_async(dispatch_get_main_queue(), ^
                 {
                    if (error)
                    {
                        handler(nil, error);
                    }
                    else
                    {
                        handler(models, nil);
                    }
                 });
             }
             else
             {
                 dispatch_async(dispatch_get_main_queue(), ^
                 {
                     handler(nil, error);
                 });
             }
             
        }] resume];
    });
}

- (NSArray *)p_parseData:(NSData *)data withError:(NSError *__autoreleasing *)error
{
    NSParameterAssert(data != nil);
    NSMutableArray *models = [NSMutableArray new];
    
    NSDictionary *resultsDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:error];
    for(NSDictionary *result in resultsDictionary[@"items"])
    {
        GitHubSegmentModel *model = [[GitHubSegmentModel alloc] initWithAvatarURL:[NSURL URLWithString:result[@"avatar_url"]] login:result[@"login"] account:result[@"html_url"]];
        [models addObject:model];
    }
    return [models copy];
}


@end
