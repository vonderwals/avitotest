//
//  SegmentInteractorTests.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 11/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

@import Foundation;

#import <XCTest/XCTest.h>

#import "SegmentInteractor.h"
#import "SegmentDataManagerInterface.h"
#import "SegmentInteractorIO.h"
#import <OCMock/OCMock.h>

@interface SegmentInteractorTests : XCTestCase

@property (nonatomic, strong) SegmentInteractor *segmentInteractor;

@property (nonatomic, strong) id output;

@property (nonatomic, strong) id dataManager;

@end

@implementation SegmentInteractorTests

- (void)setUp
{
    [super setUp];
    self.segmentInteractor = [[SegmentInteractor alloc] init];
    self.output = OCMProtocolMock(@protocol(SegmentInteractorOutput));
    self.dataManager = OCMProtocolMock(@protocol(SegmentDataManagerInterface));
    
    self.segmentInteractor.dataManager = self.dataManager;
    self.segmentInteractor.output = self.output;
}

- (void)testEmptyResultsRequest
{
    NSString *key = @"test";
    
    OCMStub([self.dataManager findDataByKey:key withCompletionHandler:([OCMArg invokeBlockWithArgs:@[], [NSNull null], nil])]);
    OCMExpect([self.output modelsLoaded:@[]]);
    
    [self.segmentInteractor findDataByKey:key];
    OCMVerifyAll(self.output);
}

- (void)tearDown
{
    [super tearDown];
}

@end
