//
//  iTunesDataManager.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import "iTunesDataManager.h"

#import "ItunesSegmentModel.h"

@implementation iTunesDataManager

static NSString * const kItunesURL = @"https://itunes.apple.com/search";

-(void)findDataByKey:(NSString *)key withCompletionHandler:(void (^) (NSArray *, NSError *)) handler
{
    NSParameterAssert(key != nil);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
    {
        NSString *handledKey = [key stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        
        NSURLComponents *components = [NSURLComponents componentsWithString:kItunesURL];
        NSURLQueryItem *term = [NSURLQueryItem queryItemWithName:@"term" value:handledKey];
        components.queryItems = @[term];
        
        [[[NSURLSession sharedSession] dataTaskWithURL:components.URL completionHandler:^(NSData * __nullable data, NSURLResponse * __nullable response, NSError * __nullable error)
          {
              if(data)
              {
                  NSError *error = nil;
                  NSArray *models = [self p_parseData:data withError:&error];
                  dispatch_async(dispatch_get_main_queue(), ^
                  {
                      if (error)
                      {
                          handler(nil, error);
                      }
                      else
                      {
                          handler(models, nil);
                      }
                  });
              }
              else
              {
                  dispatch_async(dispatch_get_main_queue(), ^
                  {
                      handler(nil, error);
                  });
              }
              
          }] resume];
    });
}

- (NSArray *)p_parseData:(NSData *)data withError:(NSError *__autoreleasing *)error
{
    NSParameterAssert(data != nil);
    NSMutableArray *models = [NSMutableArray new];
    
    NSDictionary *resultsDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:error];
    for(NSDictionary *result in resultsDictionary[@"results"])
    {
        ItunesSegmentModel *model = [[ItunesSegmentModel alloc] initWithAvatarURL:[NSURL URLWithString:result[@"artworkUrl100"]] title:result[@"trackName"] author:result[@"artistName"]];
        [models addObject:model];
    }
    return [models copy];
}

@end
