//
//  GitHubSegmentModel.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 10/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GitHubSegmentModel : NSObject

@property (nonatomic, strong, readonly) NSURL *avatarURL;

@property (nonatomic, strong, readonly) NSString *login;

@property (nonatomic, strong, readonly) NSString *account;

- (instancetype)initWithAvatarURL:(NSURL *)url login:(NSString *)login account:(NSString *)account;

@end
