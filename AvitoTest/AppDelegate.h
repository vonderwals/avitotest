//
//  AppDelegate.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

