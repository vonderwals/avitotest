//
//  LeftAvatarCell.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 10/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SegmentDisplayItem;

@interface LeftAvatarCell : UITableViewCell

@property (nonatomic, copy) void (^presentBlock)(UIImage *image, CGRect originalFrame);

- (void)setupWithDisplayItem:(SegmentDisplayItem *)item;

@end
