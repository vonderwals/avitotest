//
//  SegmenModelConverter.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 10/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SegmentModelConverter : NSObject

- (NSArray *)displayItemsFromModels:(NSArray *)models;

@end
