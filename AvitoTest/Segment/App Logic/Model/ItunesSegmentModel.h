//
//  ItunesSegmentModel.h
//  AvitoTest
//
//  Created by Alexey Shchukin on 10/07/16.
//  Copyright © 2016 AS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ItunesSegmentModel : NSObject

@property (nonatomic, strong, readonly) NSURL *avatarURL;

@property (nonatomic, strong, readonly) NSString *title;

@property (nonatomic, strong, readonly) NSString *author;

- (instancetype)initWithAvatarURL:(NSURL *)url title:(NSString *)title author:(NSString *)author;

@end
