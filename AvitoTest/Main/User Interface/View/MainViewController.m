//
//  MainMainViewController.m
//  AvitoTest
//
//  Created by Alexey Shchukin on 09/07/2016.
//  Copyright © 2016 AS. All rights reserved.
//

#import "MainViewController.h"
#import "MainModuleInterface.h"

#import <Masonry/Masonry.h>

@interface MainViewController () <UISearchBarDelegate>

@property (nonatomic, strong) UISegmentedControl *segmentedControl;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UIView *containerView;

@end

@implementation MainViewController

static NSString * const kiTunesItem = @"iTunes";
static NSString * const kGitHubItem = @"GitHub";

- (UIView *)containerView
{
    return _containerView;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _segmentedControl = [[UISegmentedControl alloc] initWithItems:@[kiTunesItem, kGitHubItem]];
        [_segmentedControl addTarget:self action:@selector(segmentChanged:) forControlEvents:UIControlEventValueChanged];
        
        _searchBar = [[UISearchBar alloc] init];
        _searchBar.delegate = self;
        _containerView = [[UIView alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self p_prepareViews];
}

- (void)p_prepareViews
{
    self.view.backgroundColor = [UIColor whiteColor];
    

    [self.view addSubview:_segmentedControl];
    
    [_segmentedControl mas_makeConstraints:^(MASConstraintMaker *make)
    {
        make.top.equalTo(self.mas_topLayoutGuideBottom);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
    
    [self.view addSubview:_searchBar];
    
    [_searchBar mas_makeConstraints:^(MASConstraintMaker *make)
    {
        make.top.equalTo(_segmentedControl.mas_bottom);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
    
    [self.view addSubview:_containerView];
    
    [_containerView mas_makeConstraints:^(MASConstraintMaker *make){
        make.top.equalTo(_searchBar.mas_bottom);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.eventHandler searchClickedAction:searchBar.text];
}

#pragma mark - Events

- (void)segmentChanged:(UISegmentedControl *)segmentedControl
{
    [self.eventHandler segmentChangedAction:segmentedControl.selectedSegmentIndex];
}

#pragma mark - MainViewInterface

- (void)updateSegmentIndex:(NSInteger)segmentIndex
{
    _segmentedControl.selectedSegmentIndex = segmentIndex;
}

@end
